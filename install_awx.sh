#!/bin/bash

# Update repositories
apt update

# Install dependencies
curl -sL https://deb.nodesource.com/setup_10.x | bash -
curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh; rm get-docker.sh
echo ""

# Information display
echo "The intallation requires to clone the AWX repository"
echo ""

# Add dir for PostgrSQL and Docker Compose
mkdir /home/pgdocker
mkdir /home/awxcompose

# Install Docker module
pip3 install docker-compose

# Clone and change directory to repository
# Install AWX
repo=https://github.com/ansible/awx.git
git clone $repo; cd awx/installer; sed -i -e "s:postgres_data_dir=/tmp/pgdocker:postgres_data_dir=\"/home/pgdocker\":g" inventory; sed -i -e "s:docker_compose_dir=\"~/.awx/awxcompose\":docker_compose_dir=\"/home/awxcompose\":g" inventory; ansible-playbook -i inventory install.yml -e ansible_python_interpreter=/usr/bin/python3 
