**Readme**

---

**General**

The scripts you find in this repository are a simple setup for installing Ansible and AWX.

The scripts where disigned for [Ubuntu 18.04 LTS](<https://www.ubuntu.com/>) and are not tested on anything else.

---

**Scripts**

The main script for installing Ansible and AWX is `setup.sh` and makes use of both `install_ansible.sh` and `install_awx.sh`. These scripts must reside in the same directory as `setup.sh`.

The `install_ansible.sh` script will install Ansible using the apt package manager.

The `install_awx.sh` script will install AWX conform the install guide provided at the [AWX repository](<https://github.com/ansible/awx/blob/devel/INSTALL.md>). Also are there some configuration options provided to determine paths.

---

**Support**

These scripts come with *absolutely no support!*

Use at own risk

---