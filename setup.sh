#!/bin/bash

# Display author information
echo "#########################################"
echo "#           Ansilbe-AWX setup           #"
echo "#                                       #"
echo "# Author:  Ricardo van der Vlag         #"
echo "# Date:    2020-03-30                   #"
echo "# Version: 2.0                          #"
echo "# Gitlab:  gitlab.com/ricardovandervlag #"
echo "#########################################"
echo ""
sleep 1

# Check for root previlliges
if [[ $(whoami) == "root" ]]; then
	echo "Starting installation..."
else
	echo "This setup requires root previlliges"
	echo "Please run this script as root:"
	echo "sudo ./setup.sh"
	exit 0
fi;

# Start Ansible installer
echo "Installing Ansible..."
./install_ansible.sh

# Start AWX installer
echo "Installing AWX..."
./install_awx.sh

echo "Thank you for using this installer. Ansible is now installed with AWX."
echo "A reboot might be required."
echo "To acces your AWX interface, go to 'http://localhost/' on this machine."
echo "To acces your AWX interface from another computer, go to 'http://$(hostname -I | awk '{print $1}')/'"
echo "Username: admin"
echo "Password: password"
