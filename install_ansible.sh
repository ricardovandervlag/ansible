#!/bin/bash

# Update repositories
apt update
# Install Python with pip
apt install -y python3-pip
# Install Docker module
pip3 install ansible
